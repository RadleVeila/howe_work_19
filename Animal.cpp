
#include <iostream>
#include <string>

class Animal
{
protected:
    std::string m_name;
    const char* m_speak;

    Animal(std::string name, const char* speak)
        : m_name(name), m_speak(speak)
    {
    }
public:
    
    std::string Voice() { return m_name; }
    const char* speak() { return m_speak; }
};
    

class Dog : public Animal
{
public:
    Dog(std::string name) : Animal(name, "Woof!") {}
    
};


class Cat : public Animal
{
public:
    Cat(std::string name) : Animal(name, "Meow!"){}
        
};


class Pig : public Animal
{
public:
    Pig(std::string name) : Animal(name, "Not my problem!"){}

};


int main()
{
    Dog dog("Dog");
    Cat cat("Cat");
    Pig pig("Pig");
   

    Animal* animals[] = { &cat, &dog, &pig, };
    for (int iii = 0; iii < 4; ++iii)
        std::cout << animals[iii]->Voice() << " says " << animals[iii]->speak() << '\n';

    return 0;
    
    
}

